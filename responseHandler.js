

function response() {
    this.Handle = function(res, result, err) {
        if (err) {
            res.send({status: 500, message: 'Database Error' + err})
        } else {
            res.send(result)
        }
    }

}

module.exports = new response();