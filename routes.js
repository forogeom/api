var access = require('./methods/access/access')
var board = require('./methods/board/board')
var category = require('./methods/category/category')
var permission = require('./methods/permission/permission')
var post = require('./methods/post/post')
var system = require('./methods/system/system')
var user = require('./methods/user/user')

module.exports = {

    configure: function(app){
        access.configure(app);
        board.configure(app);
        category.configure(app);
        permission.configure(app);
        post.configure(app);
        system.configure(app);
        user.configure(app);
    }

};