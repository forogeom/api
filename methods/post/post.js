var create = require('./create');
var get = require('./get')

module.exports = {

    configure: function(app){
        app.post('/post/create', function (req, res){
            create.Post(req, res);
        });
        app.get('/post/get/list/:board', function (req, res){
            get.GetList(req, res);
        });
        app.get('/post/get/:id', function (req, res){
            get.GetID(req, res);
        });
    }

    

};