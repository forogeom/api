var create = require('./create');
var get = require('./get')

module.exports = {

    configure: function(app){
        app.post('/user/create', function (req, res){
            create.Post(req, res);
        });
        app.get('/user/get/list', function (req, res){
            get.GetList(res);
        });
        app.get('/user/get/:id', function (req, res){
            get.GetID(req, res);
        });
    }

};