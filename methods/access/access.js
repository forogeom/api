var create = require('./create');
var get = require('./get');

module.exports = {

    configure: function(app){
        app.post('/access/create', function (req, res){
            create.Post(req, res);
        });

        app.get('/access/get/list', function(req, res){
            get.GetList(res)
        });

        
        app.get('/access/get/:id', function(req, res){
            get.GetID(req, res)
        });
    }

};