var connection = require('../../connection');
var response = require('../../responseHandler');

function get() {

  this.Get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from system', function(err, result) {
        con.release();
        response.Handle(res, result, err);
      });
    });
  };

}

module.exports = new get;
