var get = require('./get');


module.exports = {

    configure: function(app){
        app.get('/system/get', function (req, res){
            get.Get(res);
        });
    }

};