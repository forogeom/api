var create = require('./create');
var get = require('./get');

module.exports = {

    configure: function(app){

        app.post('/board/create', function (req, res){
            create.Post(req, res);
        });

        app.get('/board/get/list/:category', function (req, res){
            get.CategoryID(req, res);
        });

        app.get('/board/get/:id', function (req, res){
            get.ID(req, res);
        });
    }

};