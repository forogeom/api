var connection = require('../../connection');
var response = require('../../responseHandler');

function get() {

  this.CategoryID = function(req, res) {
    connection.acquire(function(err, con) {
      con.query('select ID, Title, Description from board where Category = ' + req.query.category, function(err, result) {
        con.release();
        response.Handle(res, result, err);
      });
    });
  };

    this.ID = function(req, res) {
    connection.acquire(function(err, con) {
      con.query('select * from board where ID = ' + req.query.id , function(err, result) {
        con.release();
        response.Handle(res, result, err);
      });
    });
  };

}

module.exports = new get;
