var connection = require('../../connection');
var response = require('../../responseHandler');

function get() {
    this.ID = function (req, res) {
        connection.acquire(function (err, con) {
            con.query('select * from category where id = ' + req.query.id, function (err, result) {
                con.release();
                response.Handle(res, result, err);
            });
        });
    }

    this.List = function (req, res) {
        connection.acquire(function (err, con) {
            con.query('select ID, Title from category', function (err, result) {
                con.release();
                response.Handle(res, result, err);
            });
        });
    }

}
module.exports = new get;