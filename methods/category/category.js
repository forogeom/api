var create = require('./create');
var get = require('./get')

module.exports = {

    configure: function(app){
        app.post('/category/create', function (req, res){
            create.Post(req, res);
        });
        app.get('/category/get/list', function(req, res){
            get.List(req, res)
        });
        app.get('/category/get/:id', function (req, res){
            get.ID(req, res);
        });
    }

};