var create = require('./create');
var get = require('./get');

module.exports = {

    configure: function(app){
        app.post('/permission/create', function (req, res){
            create.Post(req, res);
        });

        app.get('/permission/get/list', function(req, res){
            get.GetList(res)
        });

        
        app.get('/permission/get/:id', function(req, res){
            get.GetID(req, res)
        });
    }
};