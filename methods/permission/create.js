var connection = require('../../connection');
var response = require('../../responseHandler');

function create() {

     this.Post = function(req, res){
        body = req.body
        connection.acquire(function(err, con){
            con.query('insert into permission set ?', body, function(err, result){
                con.release();
        response.Handle(res, result, err);
            })
        })
    }

}

module.exports = new create;
